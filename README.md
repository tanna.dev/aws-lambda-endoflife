**Deprecated** This repo is deprecated, please move to https://gitlab.com/tanna.dev/endoflife-checker

# AWS Lambda End of Life Checker

A command-line Go tool to list all AWS Lambda functions that are in a given region, and annotating them with information around the [runtime deprecation timelines](https://docs.aws.amazon.com/lambda/latest/dg/lambda-runtimes.html).

See package documentation on [pkg.go.dev](https://pkg.go.dev/gitlab.com/tanna.dev/aws-lambda-endoflife).

## Installation

Install the command-line tool by running:

```sh
go install gitlab.com/tanna.dev/aws-lambda-endoflife@HEAD
```

## Usage

To get a Tab Separated Value (TSV) formatted output - handy for putting into a spreadsheet - you can run:

```sh
aws-lambda-endoflife -report
```

This takes advantage of AWS Configuration so will query your default profile and default region, which can be configured using the common environment variables, such as:

```sh
env AWS_DEFAULT_REGION=mars-east-1 aws-lambda-endoflife -report
```

## License

This code is licensed under the Apache-2.0.
