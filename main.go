package main

import (
	"context"
	"flag"
	"log"

	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/lambda"
	"github.com/aws/aws-sdk-go-v2/service/lambda/types"
	_ "gitlab.com/tanna.dev/aws-lambda-endoflife/runtimes"
)

func main() {
	var tsvReport bool
	flag.BoolVar(&tsvReport, "report", false, "whether to output a Tab Separated Value (TSV) formatted report")

	flag.Parse()

	if !tsvReport {
		errExit("the -report flag must be set, as we do not support human-readable output")
	}

	cfg, err := config.LoadDefaultConfig(context.Background())
	if err != nil {
		log.Fatalf("unable to load SDK config, %v", err)
	}

	svc := lambda.NewFromConfig(cfg)

	var functions []types.FunctionConfiguration
	var rows []Row
	var marker *string

	for {
		out, err := svc.ListFunctions(context.Background(), &lambda.ListFunctionsInput{
			Marker: marker,
		})
		if err != nil {
			log.Printf("unable to list functions, %v", err)
		}

		functions = append(functions, out.Functions...)
		marker = out.NextMarker

		if marker == nil {
			break
		}
	}

	for _, f := range functions {
		fout, err := svc.GetFunction(context.Background(), &lambda.GetFunctionInput{
			FunctionName: f.FunctionName,
		})
		if err != nil {
			log.Fatalf("unable to list functions, %v", err)
		}

		rows = append(rows, NewRow(cfg.Region, f, *fout))
	}

	printTSV(RowHeaders)
	for _, r := range rows {
		printTSV(r.ToCols())
	}
}
