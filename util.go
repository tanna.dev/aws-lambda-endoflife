package main

import (
	"fmt"
	"os"
	"time"
)

func safeString(s *string) string {
	if s == nil {
		return "(no value was found)"
	}

	return *s
}

func betweenDates(t1 time.Time, t2 *time.Time) int {
	hours := t2.Sub(t1).Hours()
	return int(hours) / 24
}

func PtrTo[T any](v T) *T {
	return &v
}

func errExit(err string, a ...any) {
	fmt.Fprintf(os.Stderr, err+"\n", a...)
	os.Exit(1)
}
