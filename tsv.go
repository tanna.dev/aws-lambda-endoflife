package main

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go-v2/service/lambda"
	"github.com/aws/aws-sdk-go-v2/service/lambda/types"
	"gitlab.com/tanna.dev/aws-lambda-endoflife/runtimes"
)

type Row struct {
	Region       string
	FunctionName string
	FunctionArn  string
	Runtime      string
	LastModified string
	Tags         map[string]string

	IsDeprecated      bool
	DaysToDeprecation int
	DeprecationDate   string

	IsEndOfLife     bool
	DaysToEndOfLife int
	EndOfLifeDate   string
}

func (r Row) ToCols() []string {
	cols := []string{
		r.Region,
		r.FunctionName,
		r.FunctionArn,
		r.Runtime,
		r.LastModified,

		fmt.Sprintf("%v", r.IsDeprecated),
		fmt.Sprintf("%d", r.DaysToDeprecation),
		r.DeprecationDate,

		fmt.Sprintf("%v", r.IsEndOfLife),
		fmt.Sprintf("%d", r.DaysToEndOfLife),
		r.EndOfLifeDate,
	}

	if r.Tags != nil {
		tagsBytes, err := json.Marshal(r.Tags)
		if err == nil {
			cols = append(cols, string(tagsBytes))
		}
	}

	return cols
}

var RowHeaders []string = []string{
	"Region",
	"Function Name",
	"Function ARN",
	"Runtime",
	"Last Modified",

	"Is Deprecated Runtime? (Phase 1)",
	"Days Until Deprecation (Phase 1)",
	"Deprecation Date (Phase 1)",

	"Is End Of Life Runtime? (Phase 2)",
	"Days Until End of Life (Phase 2)",
	"End of Life Date (Phase 2)",

	"Tags",
}

func NewRow(region string, fc types.FunctionConfiguration, gfo lambda.GetFunctionOutput) Row {
	row := Row{
		Region:       region,
		FunctionName: safeString(fc.FunctionName),
		FunctionArn:  safeString(fc.FunctionArn),
		Runtime:      string(fc.Runtime),
		LastModified: safeString(fc.LastModified),
		// defaults, so reporting doesn't panic when it sees it as the default of 0
		DaysToDeprecation: 100_000,
		DaysToEndOfLife:   100_000,

		Tags: gfo.Tags,
	}

	data, found := runtimes.IsDeprecated(fc.Runtime)
	if found {
		now := time.Now()

		if data.Deprecation != nil {
			row.IsDeprecated = now.After(*data.Deprecation)
			row.DeprecationDate = data.Deprecation.Format(time.RFC3339)
			row.DaysToDeprecation = betweenDates(now, data.Deprecation)
		}

		if data.EndOfLife != nil {
			row.IsEndOfLife = now.After(*data.EndOfLife)
			row.EndOfLifeDate = data.EndOfLife.Format(time.RFC3339)
			row.DaysToEndOfLife = betweenDates(now, data.EndOfLife)
		}
	}

	return row
}

func printTSV(cols []string) {
	fmt.Println(strings.Join(cols, "\t"))
}
